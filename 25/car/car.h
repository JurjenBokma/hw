#ifndef INCLUDED_CAR_
#define INCLUDED_CAR_

#include <string>

// Car class //JB: CC
class Car
{
    double speed = 0.0;
    std::string number_plate = "AI4 VXL";

    public:
        Car();

    private:
        void accelerate();
        void brake();
        double speed() const; //JB: DEMO/WC: speed already defined.
};

#endif

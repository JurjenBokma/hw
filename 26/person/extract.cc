#include "person.ih"

enum Attribute
{
    NAME,
    ADDRESS,
    PHONE,
    MASS,
};

void Person::extract(std::istream &iStream)
{
    string newLine; //JB: LOC; NAMING (not really a line).
    Attribute atr = NAME;

    while (true)
    {
        // get line delimited by ','
        if (!getline(iStream, newLine, ','))
            break;

        switch (atr)
        {
            case NAME:
                d_name = newLine;
                atr = ADDRESS;
                //JB: If you don't give the enum a type name (or if you cast
                //JB: a bit), you can increment atr in one statement per loop,
                //JB: instead of one statement per case.
                
                break;
            
            case ADDRESS:
                d_address = newLine;
                atr = PHONE;
                break;
            
            case PHONE:
                d_phone = newLine;
                //JB: DRY.
                //JB: You wrote a setPhoneNumber() function, but here you redo
                //JB: its job. When you change that function, you'll notice
                //JB: that setting the phone number by reading a record works
                //JB: differently from setting it directly. This anti-pattern
                //JB: is an endless source of only-partially-fixed bugs.
                
                atr = MASS;
                break;
            
            case MASS:
                d_mass = stoul(newLine);
                break;
        }
    }
    //JB: I might make sense to return the stream, so the caller can extract
    //JB: and  check the stream's status in a single statement.
}
// extracts a strings from the input stream
// seperated by ',' and assigns them to the right
// data members: d_name, d_address, d_phone, d_mass.

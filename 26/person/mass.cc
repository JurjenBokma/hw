#include "person.ih"

// retrieves persons mass in kg
size_t Person::mass() const
{
    return d_mass;
}
#include "person.ih"

using namespace std;

// retrieves address of person
string const &Person::address() const
{
    return d_address;
}
//JB: ?
#ifndef INCLUDED_PERSON_
#define INCLUDED_PERSON_

#include <string>

class Person
{
    std::string d_name;        // name of person
    std::string d_address;     // address field
    std::string d_phone;       // telephone number
    size_t d_mass = 0;         // the mass in kg

    public:        // member functions
    Person();
    //JB: I'd return a (const) reference as well, for chaining.
        void setName(std::string const &name);
        void setAddress(std::string const &address);
        void setPhone(std::string const &phone);
        void setMass(size_t mass);
        
        std::string const &name() const;
        std::string const &address() const;
        std::string const &phone() const;
        size_t mass() const;

        void insert(std::ostream &oStream) const;
        void extract(std::istream &iStream);
    //JB: The iosfwd header may happen to have been included via the string
    //JB: header. But you need to include it to be sure the compiler knows
    //JB: streams as well.

    private:
};
        
#endif

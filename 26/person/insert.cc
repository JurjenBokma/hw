#include "person.ih"

// prints Person's namem, address, phone number, mass
void Person::insert(ostream &oStream) const
{
    oStream << d_name << '\n' << d_address << '\n' << d_phone 
            << '\n' << d_mass << '\n';
}
//JB: So when extracting you delimit with commas, but when inserting, with
//JB: newlines... Hmmm.

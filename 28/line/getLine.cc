#include "line.ih"

// returns true if string read from input
// contains non-ws characters
bool Line::getLine(istream &iStream)
{
    string line;
    getline(iStream, line);

    //JB: CANON
    for (size_t idx = 0, len = line.length(); idx < len; ++idx)
    {
        if (line[idx] != ' ' && line[idx] != '\t') //JB: WHEEL
        {
            d_pos = 0;
            d_line = line;
            return true;
        }
    }

    return false;
}

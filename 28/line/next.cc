#include "line.ih"

// returns next non-ws character substring
string Line::next()
{
    size_t ogPos = d_pos;
    size_t len = d_line.length(); //JB: COCO

    // continue as long as there are no tabs and spaces and the end of the
    // string is not reached.
    while ((d_line[d_pos] != ' ' && d_line[d_pos] != '\t') && d_pos < len)
        ++d_pos; //JB: WHEEL

    size_t distance = d_pos - ogPos;        // index distance for substring
    //JB: COCO

    // continue as long as
    while ((d_line[d_pos] == ' ' || d_line[d_pos] == '\t') && d_pos < len)
        ++d_pos; //JB: FACTOR and WHEEL

    if (d_pos == len)
        d_pos = string::npos; //JB: :-)
    //JB: FLOW: return here and the 'else' will be SF.
    else if (d_pos == string::npos)        // no more substrings
    { //JB: SF{}
        return "";
    }

    return d_line.substr(ogPos, distance);
    //JB: std::string has special constructors for this.
}

//JB: ?
#ifndef INCLUDED_LINE_
#define INCLUDED_LINE_

#include <string>

// Line stores lines and uses its
// next() member function to retrieve each substring
// where the substrings are seperated by whitespaces
class Line
{
    size_t d_pos = 0; //JB: Naming: position of what?
    std::string d_line;        // stored string

    public:
    //JB: Inconsistent indentation.
        Line();
        bool getLine(std::istream &iStream);
        std::string next();

    private:
};
        
#endif

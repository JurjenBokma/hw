//JB: 0
#ifndef INCLUDED_CALCULATOR_
#define INCLUDED_CALCULATOR_

#include "../../29/parser/parser.h" //JB: Included, but no Parser used.
//JB: Hence the 0 score (otherwise half a point).
//JB: If you had trouble building with the Parser object files, see my posting
//JB: to the mailing list.
#include <iosfwd>

class Calculator
{
    std::string d_expr;
    char d_operator;
    double d_arg1;
    double d_arg2;
    size_t d_pos = 0; //JB: pos of what?

    public:
        void run();

    private:
    //JB: It looks like these functions don't touch any class data. Why not
    //JB: make them static then?
        double add(double num1, double num2) const;
        double subtract(double num1, double num2) const;
        double divide(double num1, double num2) const;
        double multiply(double num1, double num2) const;
        size_t modulo(size_t num1, size_t num2) const;

        bool expression();
        void evaluate();

        bool number(double *dest, bool *isInt);
        bool getOperator();

        void reset();
};

#endif

/* JB:
   I think your Calculator would benefit from a couple of private functions
   that reduce the size of your public functions.
 */

#include "calculator.ih"

bool Calculator::getOperator()
{
    d_operator = d_expr[d_pos];

    string operators = "+-/*%"; //JB: COCO, LOC, perhaps even SF.
    ++d_pos; //JB: CTR

    return operators.find(d_operator) != string::npos;
}

//JB: I think this function could be a one-liner.

#include "calculator.ih"

bool Calculator::expression()
{
    bool isInt = true;        // is num
    double num1 = 0; //JB: LOC
    double num2 = 0;

    size_t len = d_expr.length();

    // check lhs
    while (isInt && d_pos < len)
    {
        if (!number(&num1, &isInt))
        {
            return false;
        }
        
        ++d_pos;
    }

    // check operator
    if (!getOperator())
    {
        return false;
    }
    
    // ignore spaces after operator
    if (isspace(d_expr[d_pos]))
    {
        ++d_pos;
        if (isspace(d_expr[d_pos]))
            return false;
    }
    
    isInt = true;

    // check rhs
    while (isInt && d_pos < len)
    {
        //JB: FACTOR: repeat of loop above.
        if (!number(&num2, &isInt))
        {
            return false;
        }
        
        ++d_pos;
    }

    // special cases
    //JB: MR
    if (d_operator == '/' && num2 == 0)
    {
        cout << "Error: division by 0" << '\n';
        return false;
    }

    if (d_operator == '%' && (num2 <= 0 || num1 <= 0))
    {
        cout << "Error: modulo non-positive integral value" << '\n';
        return false;
    }

    d_arg1 = num1;
    d_arg2 = num2;

    return true;
} //JB: LONG
// expressions parses the stored string
// and checks whether it is valid according
// to the calculators grammar.

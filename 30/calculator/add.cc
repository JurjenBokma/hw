#include "calculator.ih"

double Calculator::add(double num1, double num2) const
{
    return num1 + num2;
}
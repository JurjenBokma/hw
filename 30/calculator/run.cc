#include "calculator.ih"

// interaction ends when user ends empty line
void Calculator::run()
{
    string input; //JB: LOC

    cout << "Please enter valid expression" << '\n'
            << "the program ends when entering an empty line" << '\n';

    while (true)
    {
        if (!getline(cin, input))
            break;
        
        if (input.empty())
            break;
        
        d_expr = input;

        if (expression())
            evaluate();
        else
            cout << "That is not a valid expression: try again!" << '\n';
        
        reset();
    }
}

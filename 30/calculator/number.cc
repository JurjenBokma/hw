#include "calculator.ih"

bool Calculator::number(double *dest, bool *isInt)
{
    if (isdigit(d_expr[d_pos])) //JB: NSC
    { 
        double num = d_expr[d_pos] - '0';
        if (num < 1e-8) //JB: NMN
            num = 0;

        *dest = (*dest) * 10 + num;        // store number
        //JB: ?!?
        
        return true;
    }

    if (isspace(d_expr[d_pos]) || d_expr[d_pos] == '\n')
    {
        *isInt = false;
        return true;
    }

    return false;
}

#include "calculator.ih"

double Calculator::multiply(double num1, double num2) const
{
    return num1 * num2;
}
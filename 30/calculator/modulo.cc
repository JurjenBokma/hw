#include "calculator.ih"

size_t Calculator::modulo(size_t num1, size_t num2) const
{
    return num1 % num2;
}
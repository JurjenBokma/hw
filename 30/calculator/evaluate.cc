#include "calculator.ih"

// select appropriate member function
// for the operator
void Calculator::evaluate()
{
    double result;

    switch(d_operator)
    {
        case '+':
            result = add(d_arg1, d_arg2);
            break;
        case '-':
            result = subtract(d_arg1, d_arg2);
            break;
        case '/':
            result = divide(d_arg1, d_arg2);
            break;
        case '*':
            result = multiply(d_arg1, d_arg2);
            break;
        case '%':
            result = modulo(d_arg1, d_arg2);
            break;
        default:
            cout << "unexpected evaluation error" << '\n';
            break;
    }

    cout << result << '\n';
}
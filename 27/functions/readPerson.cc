#include "../main.ih"

// readings each Persons' data from the standard
// input stream
void readPerson(Person personArr[], size_t size)
{
    for (size_t idx = 0; idx < size; ++idx)
    {
        cout << '?' << '\n';
        personArr[idx].extract(cin);        // read data from input stream
        //JB: What if extraction failed?
    }
}

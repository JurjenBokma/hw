#include "parser/parser.ih"

int main()
{
    Parser p;
    p.reset();
    double num = 0;
    p.number(&num);

    cout << num << '\n';
}
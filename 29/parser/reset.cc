#include "parser.ih"

// try to get newline from
// the standard input
bool Parser::reset()
{
    return d_line.getLine(cin);
}
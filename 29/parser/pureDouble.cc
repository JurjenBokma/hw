#include "parser.ih"

// convert str into double and stores it into
// the memory location stored by dest
bool Parser::pureDouble(double *dest, string const &str)
{
    size_t pos = 0;
    *dest = stod(str, &pos);

    // not all of str's chars where used to compute the double
    if (pos != 0)
        return false;

    // if dot is found in string then no integral type
    if 
    (
        // JB: IRE. Use find_first_of.
        str.find('.') != string::npos ||
        str.find('e') != string::npos ||
        str.find('E') != string::npos
    )
    {
        d_integral = false;
    }

    return true;
}

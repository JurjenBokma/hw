#include "parser.ih"

// attemps to run pureDouble
Return Parser::convert(double *dest, string const &str)
{
    try
    {
        return pureDouble(dest, str) ? NUMBER : NO_NUMBER;
    }
    catch (...)                                 // conversion failed
    {
        return NO_NUMBER;
    }
}

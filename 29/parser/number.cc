#include "parser.ih"

// returns the value stored in next substring
// (by argument)
Return Parser::number(double *dest)
{
    //JB: What if next() doesn't turn up a value?
    return convert(dest, next());
}

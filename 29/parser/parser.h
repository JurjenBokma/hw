//JB: 1
#ifndef INCLUDED_PARSER_
#define INCLUDED_PARSER_

#include "../../28/line/line.h"
#include <iosfwd>

enum Return
{
    NO_NUMBER,
    NUMBER,
    EOLN
};

// Parses parses integral types (double)
// from strings
class Parser
{
    Line d_line;
    bool d_integral;    // is it integral data type?

    public:
        Parser();
        bool reset();        // fills d_line with next input line
        Return number(double *dest);        // get value next substring
        bool isIntegral() const;
        std::string next();

    private:
        Return convert(double *dest, std::string const &str);
        bool pureDouble(double *dest, std::string const &str);
};
        
#endif

#include "parser.ih"

// returns whether stored line is a integral value
bool Parser::isIntegral() const
{
    return d_integral;
}